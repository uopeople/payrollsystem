package edu.uopeople.cs1102.mppa;


import static edu.uopeople.cs1102.mppa.PayrollSystem.kbd;
import static java.lang.System.out;

class PartTime extends Employee {

    //members
    private double rate;
    private double hoursWorked;

    PartTime(int empId, String name, Vehicle vehicle, double rate, double hoursWorked) {
        super(empId, name, vehicle);
        this.rate = rate;
        this.hoursWorked = hoursWorked;
    }

    static PartTime read() {
        out.print("Enter ID: ");
        int id = kbd.nextInt();

        out.print("\nEnter name: ");
        String name = kbd.next();

        out.print("\nEnter hourly rate: ");
        double rate = kbd.nextDouble();

        out.print("\nEnter hours worked: ");
        double hoursWorked = kbd.nextDouble();

        return new PartTime(id, name, null, rate, hoursWorked);
    }

    /**
     * @return Hourly rate
     */
    double getRate() {
        return rate;
    }

    /**
     * Sets employee's hourly rate
     * @param rate Employee's hourly rate
     */
    public void setRate(double rate) {
        this.rate = rate;
    }

    /**
     * @return work hours
     */
    double getHoursWorked() {
        return hoursWorked;
    }

    /**
     * Sets employee's work hours
     * @param hoursWorked Employee's hours worked
     */
    public void setHoursWorked(double hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    @Override
    public double calculatePay() {
        return this.getRate() * this.getHoursWorked();
    }
}
