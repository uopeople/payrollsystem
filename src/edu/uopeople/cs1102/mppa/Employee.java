package edu.uopeople.cs1102.mppa;

import static java.lang.System.out;

abstract class Employee {

    // members
    private int empId;
    private String name;
    private Vehicle vehicle;

    Employee(int empId, String name, Vehicle vehicle) {
        this.empId = empId;
        this.name = name;
        this.vehicle = vehicle;
    }

    /**
     * @return Employee ID
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * Sets the employee ID
     * @param empId Employee's ID
     */
    public void setEmpId(int empId) {
        this.empId = empId;
    }

    /**
     * @return Employee name
     */
    String getName() {
        return name;
    }

    /**
     * Sets the employee name
     * @param name Employee's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Employee vehicle
     */
    Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets employee vehicle
     * @param vehicle Employee's vehicle
     */
    void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    /**
     * Calculates total payroll for employee
     * @return total payroll for employee
     */
    public abstract double calculatePay();

    void print() {
        out.println("Employee name: " + getName());
        out.println("Has vehicle: " + (getVehicle() != null ? "Yes" : "No"));
        if (getVehicle() != null) {
            getVehicle().print();
        }
        out.println("Take home pay: " + calculatePay());
        out.println();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(this.name);
        sb.append("[").append(this.empId).append("]");
        if (this.vehicle != null) {
            sb.append("{").append(this.vehicle).append("}");
        } else {
            sb.append("{no vehicle}");
        }
        return sb.toString();
    }
}

