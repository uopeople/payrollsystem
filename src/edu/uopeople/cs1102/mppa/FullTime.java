package edu.uopeople.cs1102.mppa;


import static edu.uopeople.cs1102.mppa.PayrollSystem.kbd;
import static java.lang.System.out;

class FullTime extends Employee{

    //members
    private double salary;
    private double overtime;

    FullTime(int empId, String name, Vehicle vehicle, double salary, double overtime) {
        super(empId, name, vehicle);
        this.salary = salary;
        this.overtime = overtime;
    }

    static FullTime read() {

        out.print("Enter ID: ");
        int id = kbd.nextInt();

        out.print("\nEnter name: ");
        String name = kbd.next();

        out.print("\nEnter salary: ");
        double salary = kbd.nextDouble();

        out.print("\nEnter overtime: ");
        double overtime = kbd.nextDouble();

        return new FullTime(id, name, null, salary, overtime);
    }

    /**
     * @return Salary
     */
    double getSalary() {
        return salary;
    }

    /**
     * Sets employee's salary
     * @param salary Employee's salary
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    /**
     * @return Bonus
     */
    double getOvertime() {
        return overtime;
    }

    /**
     * Sets employee's overtime
     * @param overtime Employee's overtime
     */
    public void setOvertime(double overtime) {
        this.overtime = overtime;
    }

    @Override
    public double calculatePay() { return this.getSalary() + this.getOvertime(); }
}
