package edu.uopeople.cs1102.mppa;


import static edu.uopeople.cs1102.mppa.PayrollSystem.kbd;
import static java.lang.System.out;

class Vehicle {
    private String plateNumber;
    private String color;

    Vehicle(String plateNumber, String color) {
        this.setPlateNumber(plateNumber);
        this.setColor(color);
    }

    static Vehicle read() {
        out.print("Does this employee have a vehicle (Y/N): ");
        String hasVehicle = kbd.next();

        if (hasVehicle.equalsIgnoreCase("Y")) {
            out.print("\nEnter plate number: ");
            String plateNumber = kbd.next();

            out.print("\nEnter vehicle color: ");
            String color = kbd.next();

            return new Vehicle(plateNumber, color);
        } else {
            return null;
        }
    }

    String getPlateNumber() {
        return plateNumber;
    }

    void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    String getColor() {
        return color;
    }

    void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return this.color + "/" + this.plateNumber;
    }

    void print() {
        out.println("Plate number: " + getPlateNumber());
        out.println("Color: " + getColor());
    }
}