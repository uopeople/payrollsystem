package edu.uopeople.cs1102.mppa;

import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.System.out;

public class PayrollSystem {
    static final Scanner kbd = new Scanner(System.in);

    private static final String menu =
            "********************************\n" +
            "\t1. Add FullTime\n" +
            "\t2. Add PartTime\n" +
            "\t3. Calculate payroll\n" +
            "\t4. Exit\n" +
            "********************************";

    private ArrayList<Employee> employees = new ArrayList<>();

    public static void main(String[] args) {
        // Replacing the static use of the class
        PayrollSystem payrollSystem = new PayrollSystem();
        payrollSystem.init(); // instance init() replaced static main()
    }

    private void init() {
        out.println("Initializing payroll system...");

        String varCont = "N";
        byte menuOption;

        out.println("Payroll system initialized.");

        do {
            menuOption = showMenu();
            Employee emp = null;
            switch (menuOption) {
                case 1:
                    emp = FullTime.read();
                    break;
                case 2:
                    emp = PartTime.read();
                    break;
                case 3:
                    calcPayroll();
                    break;
                default:
                    break;
            }

            if (emp != null) {
                emp.setVehicle(Vehicle.read());
                addEmployee(emp);
            }
        } while (menuOption != 4);
    }

    /**
     * Calculates total payroll of all employees
     * @return total payroll of all employees
     */
    private double calcPayroll() {
        //removed pArrEmp parameter, since it can use the instance field employees directly
        double totalPayroll = 0;
        for (Employee employee : this.employees) {
            employee.print();
            double employeePayroll = employee.calculatePay();
            totalPayroll += employeePayroll;
        }

        out.println("-------------------------------------------\n" +
                    "Total payroll of the company: " + totalPayroll +
                    "\n-------------------------------------------");
        return totalPayroll;
    }

    /* This was moved to the FullTime class to adhere to the separation of concerns concept
    public FullTime readNewFullTime() {
        throw new NotImplementedException();
    }
    */

    /* This was moved to the PartTime class to adhere to the separation of concerns concept
    public PartTime readNewPartTime() {
        throw new NotImplementedException();
    }
    */

    private void addEmployee(Employee pEmp) {
        //removed pArrEmp parameter, since it can use the instance field employees directly
        this.employees.add(pEmp);
    }

    private byte showMenu() {
        out.println(menu);
        out.print("Input: ");
        return kbd.nextByte();
    }

    /* This was moved to the Vehicle class to adhere to the separation of concerns concept
    public Vehicle getVehicle() {
        throw new NotImplementedException();
    }
    */
}
